FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.17.0

COPY "cargo-zigbuild-bin" "/usr/local/bin/cargo-zigbuild"
